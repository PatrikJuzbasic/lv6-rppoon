﻿using System;

namespace RPPONN._6
{
    
        class Program
        {
            static void Main(string[] args)
            {
                Notebook notebook = new Notebook();
                notebook.AddNote(new Note("My Note", "Some random text"));
                notebook.AddNote(new Note("My Note_2", "Second random text"));
                notebook.AddNote(new Note("Long long long long Note Title", "Short Note"));

                Iterator iterator = new Iterator(notebook);
                iterator.Current.Show();
                iterator.Next().Show();

                do
                {
                    iterator.Current.Show();
                    iterator.Next();
                } while (iterator.IsDone != true);

                CareTaker careTaker = new CareTaker();
                ToDoItem toDo = new ToDoItem("Title", "ToDo ToDO Todo", new DateTime(2018, 1, 1));
                careTaker.AddPreviousState(toDo.StoreState());
                toDo.text = "New text rand";
                toDo.title = "New text rand2";
                careTaker.AddPreviousState(toDo.StoreState());

                Console.WriteLine(toDo.ToString());

                toDo.RestoreState(careTaker.Undo());
                Console.WriteLine(toDo.ToString());
                toDo.RestoreState(careTaker.Undo());
                Console.WriteLine(toDo.ToString());
                toDo.RestoreState(careTaker.Redo());
                Console.WriteLine(toDo.ToString());
            }
        }
    }

