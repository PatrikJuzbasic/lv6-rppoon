﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPPONN._6
{

    interface IAbstractCollection
    {
        IAbstractIterator GetIterator();
    }
}
